package vinnie.vendemia.namespace;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class RealTriviaGameActivity extends Activity {
    /** Called when the activity is first created. */
	
	TextView display;
	TextView display2;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		display = (TextView) findViewById(R.id.Answer);
		display2 = (TextView) findViewById(R.id.Answer2);
 
	}
	public void onRadioButtonClicked(View v) {
		RadioButton rb = (RadioButton) v;

		if(rb.getText().equals("7")) {
			display.setText("Correct!");
		}else {
			display.setText("Incorrect!");
		}
	}
	
	public void onRadioButtonClicked2(View v) {
		RadioButton rb = (RadioButton) v;

		if(rb.getText().equals("The nose")) {
			display2.setText("Correct!");
		}else {
			display2.setText("Incorrect!");
		}
	}
}